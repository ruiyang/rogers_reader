/*
 * Documentation for setting tags for incoming feeds
 */


 Go to Syndication -> Categories & Tags

 Choose a feed from the dropdown menu

 In Feed Categories & Tags

 	Match feed categories:
 		uncheck all options

 	Unmatched categories:
 		uncheck all options

 	Match inline tags:
 		uncheck all options

 	Unmatched inline tags:
 		Create new tags to match them
 	Filter:
 		Choose Tags

 In Categories

 	Tags:
 		Tag all pots from this feed as...
 			choose an appropriate tag, for example, use "News" for CityNews

 	Site-wide Tags:
 		Choose No. only use the categories I set up on the left.