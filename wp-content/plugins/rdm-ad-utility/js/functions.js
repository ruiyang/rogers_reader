jQuery("document").ready(function(){

	jQuery("#add_ad_util_rule_button").click(function(){
		jQuery(this).after("<img id='image_loader_add' src='/wp-content/plugins/rdm-ad-utility/images/ajax-loader.gif' />");
		jQuery.ajax({
			url: ajax.url,
			type: "POST",
			data: "action=add_ad_util_rule",
			success: function(echoed){
				add_new_row("#add_rules_container",echoed);
				jQuery("#image_loader_add").remove();
				
				add_taxonomies_event(false);
				
				jQuery(".cancel_add_ad_util_rule").click(function(){
					jQuery(this).parent().parent().remove();
				});
			}
		});
		return false;
	});
	
	add_taxonomies_event(false);
	
	jQuery(".cancel_add_ad_util_rule").click(function(){
		jQuery(this).parent().parent().remove();
	});
	
	add_delete_event();
	
	add_edit_event();
	
	jQuery("#contain table").sortable({ 
		items: ".sortable",
		cursor: "move",
		handle: ".td_move img"
	});
	
	jQuery("#save_changes_button").click(function(){

		var queryString = jQuery("#rule_page_form").serialize();
		var sure = confirm("Are you sure you want to save those changes?");
		if(sure){
			jQuery.ajax({
				url: ajax.url,
				type: "POST",
				data: "action=save_ad_util_changes&" + queryString,
				success: function(echoed){
					jQuery("#contain table").html(echoed);
					add_delete_event();
		
					add_edit_event();
					jQuery("#contain").prepend("<div class='updated fade' id='rdm_ad_util_save_changes_message' style='margin-top:10px;margin-bottom:0px;'><p>Modifications saved</p></div>");
					var timeout = setTimeout(function(){
						jQuery("#rdm_ad_util_save_changes_message").animate(
						{opacity:0},
						1500,
						function(){
							jQuery("#rdm_ad_util_save_changes_message").remove();
						});
						clearTimeout(timeout);
					},2000);
				}
			});
		}
	});
	
});

function add_cancel_edit_event()
{
	jQuery(".cancel_edit_ad_util_rule").unbind("click");
	jQuery(".cancel_edit_ad_util_rule").click(function(){	
		var current_parent = jQuery(this).parent().parent();
		var element_id_number = jQuery(current_parent).attr("id").substring(4); // Because the id equals to "row_" + number
		jQuery(this).after("<img id='image_loader_cancel_edit' src='/wp-content/plugins/rdm-ad-utility/images/ajax-loader.gif' />");
		jQuery.ajax({
			url: ajax.url,
			type: "POST",
			data: "action=cancel_edit_ad_util_rule&element=" + element_id_number,
			success: function(echoed){
				jQuery(current_parent).replaceWith(echoed);
				//jQuery("#image_loader_cancel_edit").remove();
				add_edit_event();
				add_delete_event();
				
			}
		});
		return false;
	});
}

function add_delete_event()
{
	jQuery(".delete_ad_util_rule").unbind("click");
	jQuery(".delete_ad_util_rule").click(function(){
		var delete_for_sure = confirm("Do you really want to delete this rule?");
		
		var current_parent = jQuery(this).parent().parent();
		var element_id_number = jQuery(current_parent).attr("id").substring(4); // Because the id equals to "row_" + number
		
		jQuery(this).after("<img id='image_loader_delete' src='/wp-content/plugins/rdm-ad-utility/images/ajax-loader.gif' />");
		
		if(delete_for_sure)
			jQuery.ajax({
			url: ajax.url,
			type: "POST",
			data: "action=delete_ad_util_rule&element="+element_id_number,
			success: function(echoed){
				jQuery(current_parent).remove();
				jQuery("#image_loader_delete").remove();
			}
			});
		else
			jQuery("#image_loader_delete").remove();
		
		return false;
	});
}

function add_edit_event()
{
	jQuery(".edit_ad_util_rule").unbind("click");
	jQuery(".edit_ad_util_rule").click(function(){
		var current_parent = jQuery(this).parent().parent();
		var element_id_number = jQuery(current_parent).attr("id").substring(4); // Because the id equals to "row_" + number
		
		jQuery(this).after("<img id='image_loader_edit' src='/wp-content/plugins/rdm-ad-utility/images/ajax-loader.gif' />");
		
		jQuery.ajax({
			url: ajax.url,
			type: "POST",
			data: "action=edit_ad_util_rule&element="+element_id_number,
			success: function(echoed){
				jQuery(current_parent).replaceWith(echoed);
				//jQuery("#image_loader_edit").remove();
				add_taxonomies_event(true);
				add_cancel_edit_event();
			}
		});
		
		return false;
	});
}

function add_taxonomies_event(in_edit_mode)
{
	jQuery(".taxonomy_ad_util_select").unbind("change");
	jQuery(".taxonomy_ad_util_select").change(function(){
		var current_parent = jQuery(this).parent();
		var taxonomy = jQuery(this).val();
		
		var action = "display_terms_ad_util";
		var name = jQuery(this).attr("name");
		var pattern = /[0-9]+/;
		var element_id = name.match(pattern);
		var element_query_string = "";
		
		if(in_edit_mode)
		{
			action = "display_single_mode_terms_ad_util";
			
			element_query_string = "element_id="+element_id;	
		}
		
		jQuery(this).after("<img id='image_loader_tax' src='/wp-content/plugins/rdm-ad-utility/images/ajax-loader.gif' />");
		jQuery.ajax({
			url: ajax.url,
			type: "POST",
			data: "action="+ action +"&rdm_taxonomy="+taxonomy + "&" + element_query_string,
			success: function(echoed){
				jQuery("#image_loader_tax").remove();
				current_parent.siblings(".td_terms").html(echoed);
			}
		});
	});
}

function add_new_row(table,rowcontent)
{
	if (jQuery(table).length>0)
	{
		if (jQuery(table+' > tbody').length==0) 
			jQuery(table).append('<tbody />');
			
		(jQuery(table+' > tr').length>0)?jQuery(table).children('tbody:last').children('tr:last').append(rowcontent):jQuery(table).children('tbody:last').append(rowcontent);
	
	}
}
