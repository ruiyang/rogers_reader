<?php

function rdm_ad_util_do_ajax()
{
	global $wpdb;
	$action = $_POST['action'];

	switch($action)
	{
	case "add_ad_util_rule":
		rdm_ad_util_listElement(true);
	break;
	case "display_terms_ad_util":
		$taxonomy = $_POST['rdm_taxonomy'];
		rdm_ad_util_displayTerms(true, $taxonomy);
	break;
	case "display_single_mode_terms_ad_util":
		$taxonomy = $_POST['rdm_taxonomy'];
		$element_id = $_POST['element_id'];
		rdm_ad_util_displayTerms(true, $taxonomy, 0, true, $element_id);
	break;
	case "delete_ad_util_rule":
		$element_to_delete = $_POST['element'];
		rdm_ad_util_deleteElement($element_to_delete);
	break;
	case "save_ad_util_changes":
		$data = rdm_ad_util_saveFriendlyData($_POST);
		rdm_ad_util_saveChanges($data);
		echo rdm_ad_util_getRulesPageHtml();
	break;
	case "edit_ad_util_rule":
		$element_to_edit = $_POST['element'];
		$rule = $wpdb->get_row("SELECT * FROM " . $wpdb->prefix . "rdm_ad_util_rules WHERE R_ID = " . $element_to_edit);
		rdm_ad_util_listElement(true,$rule,true);
	break;
	case "cancel_edit_ad_util_rule":
		$element_to_cancel = $_POST['element'];
		$rule = $wpdb->get_row("SELECT * FROM " . $wpdb->prefix . "rdm_ad_util_rules WHERE R_ID = " . $element_to_cancel);
		rdm_ad_util_listElement(true,$rule);
	break;
	}
	exit;
}

function rdm_ad_util_saveFriendlyData($data)
{
	$return_data = array();
	foreach($data as $key => $value)
	{
		if($key != "action" && $key != "element")
		{
			$return_data[$key] = $value;
		}
	}
	return $return_data;
}

function rdm_ad_util_saveNewRules($data)
{
	global $wpdb;
	$datakeys = array_keys($data);
	
	$data_to_insert = array();
	
	$i = 0;
	
	foreach($data as $key => $value)
	{
		foreach($value as $thing)
		{
			if(!isset($data_to_insert[$i]["R_order"]))
				$data_to_insert[$i]["R_order"] = $i + 1;
				
			if($key == "rdm_taxonomy"){
				$key = "taxonomy";
			}
			$data_to_insert[$i]["R_".$key] = $thing;
			$i++;
		}
		$i = 0;
	}
	
	
	$empty_rules = 0;
	
	$invalid_rules = array();
	$valid_rules = array();
	
	foreach($data_to_insert as $data_array)
	{
		if($data_array["R_taxonomy"] == "")
		{
			$empty_rules++;
		}
		else if (($data_array["R_taxonomy"] == "path" && $data_array["R_path"] == "") || $data_array["R_zone_name"] == "")
		{
			$invalid_rules[] = $data_array;
		}
		else
		{
			$valid_rules[] = $data_array;
		}
	}
	
	$nb_invalid_rules = count($invalid_rules);
	$nb_valid_rules = count($valid_rules);
	
	$wpdb->query("UPDATE " . $wpdb->prefix . "rdm_ad_util_rules SET R_order = R_order + " . $nb_valid_rules); // Readjust order of other rules
	
	foreach($valid_rules as $valid_rule) {
		
		$insert = $wpdb->insert($wpdb->prefix . "rdm_ad_util_rules",$valid_rule);
	
	}
	
	if($empty_rules > 0)
	{
		if($empty_rules > 1) // Plural outputs
		{
		$output_be="are";
		$output_rule="rules";
		$output_it="them";
		}
		else // Singular outputs
		{
		$output_be="is";
		$output_rule="rule";
		$output_it="it";
		}
		
		echo "<div class='updated fade' id='message'><p>There {$output_be} {$empty_rules} empty {$output_rule} in your request, the plugin will not take care of {$output_it}.</p></div>";
	}
	
	if($nb_valid_rules > 0)
	{
		if($nb_valid_rules > 1)
			$output_rule="rules";
		else
			$output_rule="rule";
			
		echo "<div class='updated fade' id='message'><p>{$nb_valid_rules} {$output_rule} has been inserted</p></div>";
	}
	
	if($nb_invalid_rules > 0)
	{
		if($nb_invalid_rules > 1)
		{
		$output_be="are";
		$output_rule="rules";
		}
		else
		{
		$output_be="is";
		$output_rule="rule";
		}
		
		echo "<div class='error fade' id='message'><p>There {$output_be} {$nb_invalid_rules} {$output_rule} that contain errors. Revise and resave them if needed.</p></div>";
		return $invalid_rules;
	}
	
	return false;
	
}

function rdm_ad_util_saveChanges($data) {

	global $wpdb;
	
		
	$updated_rows = array();
	foreach($data as $key => $value) {
		if($key == "order"){
			$order = 1;
			foreach($value as $element_id){
				$updated_rows[$element_id]["R_".$key] = $order;
				$order++;
			}
		}
		else{
			foreach($value as $id => $v){
				if($key == "rdm_taxonomy"){
					$key = "taxonomy";
				}
				$updated_rows[$id]["R_".$key] = $v;
			}
		}
	}
	
	foreach($updated_rows as $rule_id => $updated_data) {
		$wpdb->update($wpdb->prefix . "rdm_ad_util_rules",$updated_data,array("R_ID"=>$rule_id));
	}
}

function rdm_ad_util_deleteElement($element)
{
	global $wpdb;
	$wpdb->query("DELETE FROM " . $wpdb->prefix . "rdm_ad_util_rules WHERE R_ID = " . $element);
}

function rdm_ad_util_registerSettings() {

	register_setting('rdm_ad_util_settingsGroup','rdm_ad_util_settings','rdm_ad_util_validateSettings');

}

function rdm_ad_util_validateSettings($passedSettings) {

	$errors = "";
	
	$allow_interstitials = false;

	foreach($passedSettings as $setting=>$value) {
	
		if($setting == "site_name") {
			if($value == "") {
				$errors .= " - You must enter a site name<br />";
			}
		}
		else if($setting == "home_zone_name") {
			if($value == "") {
				$errors .= " - You must enter a homepage zone name<br />";
			}
		}
		else if($setting == "allow_interstitials") {
			$allow_interstitials = true;
		}
	}
	
	if($errors != "") {
		add_settings_error('rdm_ad_util_settings','rdm-ad-util-errors','<strong>There are errors in your request</strong> : <br />' . $errors,'error');
	}
	
	$passedSettings['allow_interstitials'] = $allow_interstitials;

	return $passedSettings;
}


function rdm_ad_util_settingsNotSet() {
	if(isset($_GET['settings-updated']) || (isset($_GET['page']) && $_GET['page'] == "rdm_ad_utility_options_page")) {
		echo "<div class='error'><p><strong>RDM Ad Utility</strong> contains errors. Site name and homepage zone name are required settings, please set them.</p></div>";
	}
	else {
		echo "<div class='error'><p><strong>RDM Ad Utility</strong> settings has not been set. Site name and homepage zone name are required settings. Click <a href='admin.php?page=rdm_ad_utility_options_page'>here</a> to set them.</p></div>";
	}
}

function rdm_ad_util_checkOptions()
{
	
	$rdm_ad_util_settings = get_option("rdm_ad_util_settings");
	
	if($rdm_ad_util_settings['site_name'] == "" || $rdm_ad_util_settings['home_zone_name'] == "" ) {
		add_action( 'all_admin_notices', 'rdm_ad_util_settingsNotSet' );
	}
	else {
		remove_action( 'all_admin_notices', 'rdm_ad_util_settingsNotSet' );
	}
}

/********************
*****
Template functions
*****
********************/

function rdm_ad_util_settingsPage()
{	
	
	$rdm_ad_util_settings = get_option("rdm_ad_util_settings");
	
	settings_errors();
	?>
	<div id="contain" class="wrap">
	<h2>Rogers Digital Media - Ad Utility - Manage options</h2>
    
    

    <form name="options_form" action="options.php" method="post">
    <?php 
		settings_fields('rdm_ad_util_settingsGroup'); 
		
		$redText = "style='color:red;'";
		$redHighlight = "style='border:1px solid red;'";
	?>
    <table class="form-table">
    	 <tr valign="top">
            <th scope="row"><label for="rdm_ad_util_settings[allow_interstitials]">Allow interstitials</label></th>
            	<td><input type='checkbox' name='rdm_ad_util_settings[allow_interstitials]' id="allow_interstitials" value="1" <?php (isset($rdm_ad_util_settings['allow_interstitials']))?checked(1,$rdm_ad_util_settings['allow_interstitials']):""; ?> />
				
            </td>
        </tr>
        <tr valign="top">
            <th scope="row"><label for="rdm_ad_util_settings[site_name]">Site name</label></th>
            	<?php
                	if($rdm_ad_util_settings['site_name'] == "") {	
						echo "<td $redText><input $redHighlight type='text' name='rdm_ad_util_settings[site_name]' id='site_name' value='" . $rdm_ad_util_settings['site_name'] . "' /> <em>You must enter a site name</em>";
					}
					else {
						echo "<td><input type='text' name='rdm_ad_util_settings[site_name]' id='site_name' value='" . $rdm_ad_util_settings['site_name'] . "' /> Example : rogers.publishing/Chatelaine.CPGWomen";
					}
				?>
            </td>
        </tr>
        <tr valign="top">
            <th scope="row"><label for="rdm_ad_util_settings[home_zone_name]">Homepage zone name</label></th>
            <?php
				if($rdm_ad_util_settings['home_zone_name'] == "") {	
					echo "<td $redText><input $redHighlight  type='text' name='rdm_ad_util_settings[home_zone_name]' id='home_zone_name' value='" . $rdm_ad_util_settings['home_zone_name'] . "' /> <em>You must enter an homepage zone name</em>";
				}
				else {
					echo "<td><input type='text' name='rdm_ad_util_settings[home_zone_name]' id='home_zone_name' value='" . $rdm_ad_util_settings['home_zone_name'] . "' />";
				}
			?>
        </tr>
        <tr valign="top">
            <th scope="row"><label for="rdm_ad_util_settings[home_keywords]">Homepage keywords</label></th>
            <td><input type="text" name="rdm_ad_util_settings[home_keywords]" id="home_keywords" value="<?php echo $rdm_ad_util_settings['home_keywords']; ?>"/></td>
        </tr>
    </table><br /> 
    <input type="submit" class="button-primary" value="Save options" />
	</form>
	</div>
	<?php
}

function rdm_ad_util_addRulesPage()
{
	$invalid_rules = false;
	
	if(isset($_POST))
	{
		$invalid_rules = rdm_ad_util_saveNewRules($_POST);
	}
	
	$html = "<div id='contain' class='wrap'><h2>Rogers Digital Media - Ad Utility - Add Rules</h2>";
	$html .= "<br />";
	
	$html .= "<a href='#' id='add_ad_util_rule_button' class='button-secondary'>Add rule</a>";
	
	$html .= "<form name='add_rules_form' action='" . $_SERVER['REQUEST_URI'] . "' method='post'>";
	$html .= "<table style='width:100%' id='add_rules_container' class='widefat ad-util-table'>";
	$html .= "<tr class='head'>";
	$html .= "<th style='width:2%;'> </th><th style='width:19%;'>Taxonomy</th><th style='width:19%;'>Element</th><th style='width:19%;'>Zone</th><th style='width:19%;'>Keyword</th><th style='width:19%;'> </th>";
	$html .= "</tr>";
	
	if($invalid_rules != false)
	{
		foreach($invalid_rules as $rule)
		{
			$html .= rdm_ad_util_listElement(false,(object)$rule,false,true);
		}
	}
	else
	{
		$html .= rdm_ad_util_listElement(false);
	}
	
	$html .= "</table>";
	$html .= "<br />";
	$html .= "<input type='submit' value='Save rules' class='button-primary' />";
	
	$html .= "</form></div>";
	
	echo $html;
}

function rdm_ad_util_seeRulesPage()
{
	
	$html = "<div id='contain' class='wrap'>";
	
	$html .= "<h2>Rogers Digital Media - Ad Utility - Manage rules</h2>";
	
	$html .= "<form name='rule_page_form' id='rule_page_form' method='post'>";
	

	$html .= "<table style='width:100%' class='widefat ad-util-table'>";
	
	$html .= rdm_ad_util_getRulesPageHtml();
	
	$html .= "</table>";
	
	$html .= "</form>";
	$html .= "<br />";
	$html .= "<a href='#' id='save_changes_button' class='button-primary'>Save all</a>";
	
	$html .= "</div>";
	
	echo $html;
	
}

function rdm_ad_util_getRulesPageHtml(){

	global $wpdb;
	
	$html = "";
	
	$html .= "<tr class='head'>";
	$html .= "<th style='width:2%;'></th><th style='width:19%;'>Taxonomy</th><th style='width:19%;'>Element</th><th style='width:19%;'>Zone</th><th style='width:19%;'>Keyword</th><th style='width:19%;'> </th>";
	$html .= "</tr>";
	
	$rules = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "rdm_ad_util_rules ORDER BY R_order");
	
	if($rules)
	{
		foreach($rules as $rule)
		{
			$html .= rdm_ad_util_listElement(false, $rule);
		}
	}
	return $html;
}

function rdm_ad_util_listElement($must_echo, $rule = '', $in_edit_mode = false, $is_invalid = false)
{
	global $wpdb;
	
	$html = "";
	
	if($rule != '') // When we want to display an already existing element or an invalid one
	{
		
		$term = get_term_by('id',$rule->R_term_id,$rule->R_taxonomy);
		
		if($is_invalid) { // When the user try to create an invalid rule.
			$html .= "<tr class='sortable'>";
			$html .= "<td class='td_move'><img src='" . plugins_url("images/move.png",__FILE__) . "' width='17' /></td>";
			$html .= "<td class='td_taxonomies'>" . rdm_ad_util_displayTaxonomies(false, $rule->R_taxonomy) . "</td>";
			$html .= "<td class='td_terms'>" . rdm_ad_util_displayTerms(false,$rule->R_taxonomy,$rule->R_term_id) . "</td>";
			$html .= "<td class='td_zone_name'><input type='text' name='zone_name[]' value='" . $rule->R_zone_name . "' /></td>";
			$html .= "<td class='td_keywords'><input type='text' name='keywords[]' value='" . $rule->R_keywords . "' /></td>";
			$html .= "<td>";
			$html .= "<a href='#' class='cancel_add_ad_util_rule'>Cancel</a></td>";
			$html .= "</tr>";
		}
		else if($in_edit_mode) { // When the element is being edited
			$html .= "<tr id='row_" . $rule->R_ID  . "' class='sortable'>";
			$html .= "<td class='td_move'><img src='" . plugins_url("images/move.png",__FILE__) . "' width='17' /></td>";
			$html .= "<td class='td_taxonomies'>" . rdm_ad_util_displayTaxonomies(false, $rule->R_taxonomy,$in_edit_mode, $rule->R_ID) . "</td>";
			if($rule->R_taxonomy == "path")
			{
				$html .= "<td class='td_terms'><input type='text' name='path[" . $rule->R_ID . "]' id='path_" . $rule->R_ID . "' value='" . $rule->R_path . "' /></td>";
			}
			else
			{
				$html .= "<td class='td_terms'>" . rdm_ad_util_displayTerms(false,$rule->R_taxonomy,$rule->R_term_id,$in_edit_mode,$rule->R_ID) . "</td>";
			}
			$html .= "<td class='td_zone_name'><input type='text' name='zone_name[" . $rule->R_ID . "]' id='zone_name_" . $rule->R_ID . "' value='" . $rule->R_zone_name . "' /></td>";
			$html .= "<td class='td_keywords'><input type='text' name='keywords[" . $rule->R_ID . "]' id='keywords_" . $rule->R_ID . "' value='" . $rule->R_keywords . "' /></td>";
			$html .= "<td>";
				
			$html .= "<a href='#' id='cancel_" . $rule->R_ID . "' class='cancel_edit_ad_util_rule'>Cancel</a>";
			$html .= "<input type='hidden' name='order[]' value='" . $rule->R_ID . "' /></td>";
			
			$html .= "</tr>";
		}
		else { // When we just display the element
			$html .= "<tr id='row_" . $rule->R_ID  . "' class='sortable'>";
			$html .= "<td class='td_move'><img src='" . plugins_url("images/move.png",__FILE__) . "' width='17' /></td>";
			$html .= "<td>" . $rule->R_taxonomy . "</td>";
			
			if($rule->R_taxonomy == "path")
				$html .= "<td>" . $rule->R_path . "</td>";
			else
				$html .= "<td>" . $term->name . "</td>";
				
			$html .= "<td>" . $rule->R_zone_name . "</td><td>" . $rule->R_keywords . "</td><td><a href='#' id='edit-" . $rule->R_ID . "' class='edit_ad_util_rule'>Edit</a>&nbsp;&nbsp;<a href='#' id='delete-" . $rule->R_ID . "' class='delete_ad_util_rule'>Delete</a>";
			$html .= "<input type='hidden' name='order[]' value='" . $rule->R_ID . "' /></td>";
			$html .= "</tr>";
		}
		
	}
	else // When we add a new element
	{
		$html .= "<tr class='sortable'>";
		$html .= "<td class='td_move'><img src='" . plugins_url("images/move.png",__FILE__) . "' width='17' /></td>";
		$html .= "<td class='td_taxonomies'>" . rdm_ad_util_displayTaxonomies(false) . "</td>";
		$html .= "<td class='td_terms'></td>";
		$html .= "<td class='td_zone_name'><input type='text' name='zone_name[]' /></td>";
		$html .= "<td class='td_keywords'><input type='text' name='keywords[]' /></td>";
		$html .= "<td><a href='#' class='cancel_add_ad_util_rule'>Cancel</a></td>";
		$html .= "</tr>";
	}
	
	if($must_echo)
		echo $html;
	else
		return $html;
}

function rdm_ad_util_displayTaxonomies($must_echo, $selected_tax = '', $in_edit_mode = false, $rule_ID = "")
{
	$string_list = "";
	
	
	$taxonomies = get_taxonomies();
	
	if($in_edit_mode)
		$html = "<select name='rdm_taxonomy[" . $rule_ID . "]' class='taxonomy_ad_util_select'>";
	else
		$html = "<select name='rdm_taxonomy[]' class='taxonomy_ad_util_select'>";
	
	$html .= "<option value=''>Make a choice</option>";
	$html .= "<option value=''>------------</option>";
	foreach($taxonomies as $taxonomy)
	{
		
		$terms = get_terms($taxonomy,array('hide_empty'=>false,'fields'=>'count'));
		
		if($terms > 0) {
			$html .= "<option value='$taxonomy'";
			if($selected_tax == $taxonomy)
				$html .= " selected";
			$html .= ">" . ucfirst($taxonomy) . "</option>";
		}
	}
	
	$html .= "<option value=''>------------</option>";
	$html .= "<option value='path'";
	if($selected_tax == 'path')
		$html .= " selected";
		
	$html .= ">Relative path</option>";
	
	$html .= "</select>";
	
	if($must_echo)
		echo $html;
	else
		return $html;
}

function rdm_ad_util_displayTerms($must_echo, $taxonomy, $term_id = 0, $in_edit_mode = false, $rule_ID = "")
{

	if($in_edit_mode)
	{
		$select_name = "term_id[" . $rule_ID . "]";
		$path_input_name = "path[" . $rule_ID . "]";
	}
	else
	{
		$select_name = "term_id[]";
		$path_input_name = "path[]";
	}
	
	if($taxonomy == ""){
		$html = "";
	}
	else {
		
		if($taxonomy == "path") {
			$html = "<input type='hidden' name='" . $select_name . "' />";
			$html .= "<input type='text' name='" . $path_input_name . "' id='path'/>";
		}
		else {
			
			$taxonomy_object = get_taxonomy($taxonomy);
			
			$args = array("taxonomy" => $taxonomy, "orderby" => "term_name", "hide_empty" => false, "class" => "term", "echo" => false, "name" => $select_name, "id" => " ", "hierarchical" => $taxonomy_object->hierarchical);
				
			if($term_id > 0)
				$args["selected"] = $term_id;
			
			$html = wp_dropdown_categories($args);
			$html .= "<input type='hidden' name='" . $path_input_name . "' id='path'/>";
		}
		
	}
	
	if($must_echo)
		echo $html;
	else
		return $html;
}

/************** Template functions end **************/
?>