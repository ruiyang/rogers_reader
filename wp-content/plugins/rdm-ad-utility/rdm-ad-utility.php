<?php 
/*
Plugin Name: RDM Ad Utility
Description: Plugs in the necessary elements for Ad Utility and gives you an easy way to manage your zone and keywords.
Version: 3.2
Author: Marc-Andre Beaudry
*/
define("RDM_AD_UTIL_VERSION","3.2");

require_once("rdm-ad-util-functions.php");

if(isset($_GET['page'])) {
	if(strstr($_GET['page'],'rdm_ad_utility')) {
			
		add_action('admin_enqueue_scripts','rdm_ad_util_addScript');
		add_action('admin_init','rdm_ad_util_addStyle');
		
	}
}

add_action('admin_menu','rdm_ad_util_adminMenu');

add_action('admin_init','rdm_ad_util_checkOptions');
add_action('admin_init','rdm_ad_util_addAction');
add_action('admin_init','rdm_ad_util_registerSettings');

add_action('plugins_loaded','rdm_ad_util_checkDbUpdate');

add_action("wp_head","rdm_ad_util_headerCode");
add_action("wp_footer","rdm_ad_util_footerCode");

register_activation_hook(__FILE__,"rdm_ad_util_install");

$errorsArray = array();

function rdm_ad_util_addAction() {
	
	add_action("wp_ajax_add_ad_util_rule", 'rdm_ad_util_do_ajax' );
	add_action("wp_ajax_display_terms_ad_util", 'rdm_ad_util_do_ajax' );
	add_action("wp_ajax_display_single_mode_terms_ad_util", 'rdm_ad_util_do_ajax' );
	add_action("wp_ajax_delete_ad_util_rule", 'rdm_ad_util_do_ajax' );
	add_action("wp_ajax_edit_ad_util_rule", 'rdm_ad_util_do_ajax' );
	add_action("wp_ajax_cancel_edit_ad_util_rule", 'rdm_ad_util_do_ajax' );
	add_action("wp_ajax_save_ad_util_changes", 'rdm_ad_util_do_ajax' );
}

function rdm_ad_util_install() {
	global $wpdb;
	
	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	
	$rdm_ad_util_settings = get_option("rdm_ad_util_settings");
	
	$rdm_ad_util_db_version = get_option('rdm_ad_util_db_version',"1.0");
	
	$charset = "";
	
	$rdm_ad_util_table_name = $wpdb->prefix . "rdm_ad_util_rules";
	
	if (version_compare(mysql_get_server_info(), '4.1.0', '>=')) {
        if (!empty($wpdb->charset)) {
            $charset = "DEFAULT CHARACTER SET $wpdb->charset";
        }

        if (!empty($wpdb->collate)) {
            $charset .= " COLLATE $wpdb->collate";
        }
    }
	
	if($wpdb->get_var("SHOW TABLES LIKE '$rdm_ad_util_table_name'") != $rdm_ad_util_table_name) {
		
		$sql = "CREATE TABLE IF NOT EXISTS $rdm_ad_util_table_name (
		R_ID BIGINT NOT NULL AUTO_INCREMENT,
		R_taxonomy VARCHAR(50) NOT NULL,
		R_term_id BIGINT NOT NULL,
		R_path VARCHAR(150) NOT NULL, 
		R_zone_name VARCHAR(50) NOT NULL,
		R_keywords TEXT NOT NULL,
		R_order INT NOT NULL,
		PRIMARY KEY (R_ID)
		) $charset;";
		
		dbDelta($sql);
	}	
	else {
		
		if(version_compare($rdm_ad_util_db_version,"1.1","<")) {
					
			$wpdb->query("ALTER TABLE {$rdm_ad_util_table_name} ADD R_order INT NOT NULL");
			
			$all_rules_id = $wpdb->get_results("SELECT R_id FROM {$rdm_ad_util_table_name} ORDER BY R_id DESC");
			
			$order = 0;
			
			foreach($all_rules_id as $rule) {
				
				$order++;
				$wpdb->update($rdm_ad_util_table_name,array("R_order"=>$order),array("R_id"=>$rule->R_id));
			
			}
		}
	}
	
	rdm_ad_util_initSettings();
	
}

function rdm_ad_util_checkDbUpdate() {
	
	$rdm_ad_util_db_version = get_option('rdm_ad_util_db_version','1.0');
	
	if(version_compare($rdm_ad_util_db_version,RDM_AD_UTIL_VERSION,"!=")){
		
		rdm_ad_util_install();
	
	}
}

function rdm_ad_util_initSettings()
{
	$rdm_ad_util_settings = get_option("rdm_ad_util_settings"); // Version 3.2 settings handle
	$rdm_ad_util_options = get_option("rdm_ad_util_options"); // Older version settings handle
	
	$rdm_ad_util_db_version = get_option("rdm_ad_util_db_version","1.0");
	
	$settings['site_name'] = "";
	$settings['home_zone_name'] = "homepage";
	$settings['home_keywords'] = "";
	
	if(!empty($rdm_ad_util_options) && empty($rdm_ad_util_settings)) {
		
		$rdm_ad_util_settings['site_name'] = $rdm_ad_util_options['site_name'];
		$rdm_ad_util_settings['home_zone_name'] = $rdm_ad_util_options['home_zone_name'];
		$rdm_ad_util_settings['home_keyword'] = $rdm_ad_util_options['home_keyword'];
		
		delete_option('rdm_ad_util_options');
		add_option('rdm_ad_util_settings',$rdm_ad_util_settings);
		
	} 
	else if(empty($rdm_ad_util_settings) && empty($rdm_ad_util_options)){
		
		add_option("rdm_ad_util_settings",$settings);
		add_option("rdm_ad_util_db_version",RDM_AD_UTIL_VERSION);
	}
}

function rdm_ad_util_adminMenu() {
	
	add_menu_page(
		"RDM - Ad Utility - See/edit rules",	// Page title
		"RDM Ad Utility",							// Menu title
		"administrator",						// Capability
		"rdm_ad_utility",						// Handle
		"rdm_ad_util_seeRulesPage"			// Function
	);
	
	add_submenu_page(
		"rdm_ad_utility",						// Parent handle
		"RDM - Ad Utility - See/edit rules",	// Page title
		"See/edit rules",						// Menu title
		"administrator",						// Capability
		"rdm_ad_utility",						// Handle
		"rdm_ad_util_seeRulesPage"			// Function
	);
	
	add_submenu_page(
		"rdm_ad_utility",						// Parent handle
		"RDM - Ad Utility - Add rules",			// Page title
		"Add rules",							// Menu title
		"administrator",						// Capability
		"rdm_ad_utility_add_rules",				// Handle
		"rdm_ad_util_addRulesPage"			// Function
	);
	add_submenu_page(
		"rdm_ad_utility",						// Parent handle
		"RDM - Ad Utility - Options",			// Page title
		"Options",								// Menu title
		"administrator",						// Capability
		"rdm_ad_utility_options_page",			// Handle
		"rdm_ad_util_settingsPage"				// Function
	);
}

function rdm_ad_util_addScript()
{
	$wp_version = get_bloginfo('version');
	if(version_compare($wp_version,'3.5','<')){ 
		wp_enqueue_script('jquery_ui', plugins_url('/js/jquery-ui-1.8.16.custom.min.js',__FILE__));
	}
	else {
		wp_enqueue_script('jquery-ui-core');
		wp_enqueue_script('jquery-ui-sortable');
	}
	
	wp_enqueue_script('rdm_ad_util_plugin_js', plugins_url('/js/functions.js',__FILE__));
	wp_localize_script( 'rdm_ad_util_plugin_js', 'ajax', array( "url" => admin_url( 'admin-ajax.php' )) );
}

function rdm_ad_util_addStyle()
{
	wp_enqueue_style('rdm_ad_util_plugin_css', plugins_url('/style/styles.css',__FILE__));
}

function rdm_ad_util_getRuleForPage()
{
	global $wpdb, $wp_query;
	
	$rdm_ad_util_settings = get_option("rdm_ad_util_settings");
	
	$default_rule['R_zone_name'] = "/";
	$default_rule['R_keywords'] = "";
	
	$home_rule['R_zone_name'] = $rdm_ad_util_settings['home_zone_name'];
	$home_rule['R_keywords'] = $rdm_ad_util_settings['home_keywords'];
	
	$rules = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "rdm_ad_util_rules ORDER BY R_order");
	
	$inherited_rule = "";
	
	foreach($rules as $rule)
	{
		if($rule->R_taxonomy == "path")
		{
			$relative_path = $_SERVER['REQUEST_URI'];
			
			if(strpos($relative_path,'?') != false) {
				$querystring_pos = strpos($relative_path,'?');
				$relative_path = substr($_SERVER['REQUEST_URI'],0,$querystring_pos);
			}
			
			if(strpos($rule->R_path,'*') != false){
				$reg_ex = "";
				$reg_ex = str_replace('*','(.*)',$rule->R_path);
				
				$reg_ex = "/^" . str_replace('/','\/',$reg_ex) . "$/";
				
				if(preg_match($reg_ex,$relative_path)){
					return $rule;
				}
			}
			
			if($relative_path == $rule->R_path)
				return $rule;
		}
		else
		{
			if(is_single()){
				
				$term_children = get_term_children($rule->R_term_id,$rule->R_taxonomy);
				
				if(has_term($rule->R_term_id,$rule->R_taxonomy))
					return $rule;
				
				if(!empty($term_children)){
					foreach($term_children as $term_child){
						if(has_term($term_child,$rule->R_taxonomy)){
							return $rule;
						}
					}
				}
			}
			else {
								
				if(is_archive()) {				
					
					$current_term_id = $wp_query->get_queried_object_id();
					
					if($current_term_id == $rule->R_term_id)
						return $rule;
				}
			}
		}
	}
	
	if(is_home()) {
		return (object)$home_rule;
	}
	else {
		return (object)$default_rule;
	}
	
}

function rdm_ad_util_headerCode() {
	
	$rdm_ad_util_settings = get_option("rdm_ad_util_settings");
	
	$rule = rdm_ad_util_getRuleForPage();
	
	?>
	<!-- Ad Utility source code -->
    <script type="text/javascript" src="http://static.rogersdigitalmedia.com/libs/RDMAdUtility4/current/rdm-ad-util.min.js"></script> 

	<script type='text/javascript'>	
		window.adUtility = new RDMAdUtility({
			site: '<?php echo $rdm_ad_util_settings['site_name']; ?>',
			zone: '<?php echo $rule->R_zone_name; ?>',
			sponsorshipId: '<?php echo $rule->R_keywords; ?>'
		});
	</script>
<?php
}


function rdm_ad_util_footerCode() {
?>
	<div id="dhtml-ad-hook">
	  <script type="text/javascript">
	  	if(document.documentElement.clientWidth >= 1008){
			adUtility.insertAd("dhtml-ad-hook", { type: adUtility._AD_DHTML });
	  	}
	  </script>
	</div>
<?php
}
?>
