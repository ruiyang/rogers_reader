<?php
/*
Plugin Name: User Feeds Selector
Plugin URI: http://localhost
Description: Enable User Feed Preference Meta
Version: 1.0
Author: Leon Qi
Author URI: http://localhost
*/


add_action( 'show_user_profile', 'rr_feed_profile_fields' );
add_action( 'edit_user_profile', 'rr_feed_profile_fields' );

function rr_feed_profile_fields( $user ) { 
	
	$feed_preference = get_the_author_meta( 'feed_preference', $user->ID );
	
	//print_r($feed_preference);
	
	?>

	<h3>Feeds Preference</h3>

	<div class="user-pref">
		<div class="cat-container">
			<div class="main-cat">
				<input type="hidden" name="feed_preference[news]" id="_cat-news" value="0"/>
				<input type="checkbox" name="feed_preference[news]" class="cb-main-cat" id="cat-news" value="1" <?php if(isset($feed_preference['news'])&&$feed_preference['news']==1){?>checked="checked" <?php }?>/>
				<label for="cat-news">News</label>
			</div>
			<!--
			<div class="sub-cat">
				<div class="sub-cat-wrap">
					<input type="hidden" name="preference['news']['citynews']" id="_cat-citytv" value="0"/>
					<input type="checkbox" name="preference['news']['citynews']" class="cb-sub-cat" id="cat-citytv"/>
					<label for="cat-news">CityTV</label>
				</div>
			</div>
			-->
			
		</div>
		<div class="cat-container">
			<div class="main-cat">
				<input type="hidden" name="feed_preference[sports]" id="_cat-sports" value="0"/>
				<input type="checkbox" name="feed_preference[sports]" class="cb-main-cat" id="cat-sports" value="1" <?php if(isset($feed_preference['sports'])&&$feed_preference['sports']){?>checked="checked" <?php }?>/>
				<label for="cat-sports">Sports</label>
			</div>
		</div>
		<div class="cat-container">
			<div class="main-cat">
				<input type="hidden" name="feed_preference[business]" id="_cat-business-" value="0"/>
				<input type="checkbox" name="feed_preference[business]" class="cb-main-cat" id="cat-business-finance" value="1" <?php if(isset($feed_preference['business'])&&$feed_preference['business']){?>checked="checked" <?php }?>/>
				<label for="cat-business-finance">Business</label>
			</div>
		</div>
		<div class="cat-container">
			<div class="main-cat">
				<input type="hidden" name="feed_preference[finance]" id="_cat-finance" value="0"/>
				<input type="checkbox" name="feed_preference[finance]" class="cb-main-cat" id="cat-finance" value="1" <?php if(isset($feed_preference['finance'])&&$feed_preference['finance']){?>checked="checked" <?php }?>/>
				<label for="cat-finance">Finance</label>
			</div>
		</div>
		<div class="cat-container">
			<div class="main-cat">
				<input type="hidden" name="feed_preference[womens]" id="_cat-womens" value="0"/>
				<input type="checkbox" name="feed_preference[womens]" class="cb-main-cat" id="cat-womens" value="1" <?php if(isset($feed_preference['womens'])&&$feed_preference['womens']){?>checked="checked" <?php }?>/>
				<label for="cat-womens">Women's</label>
			</div>
		</div>
		<div class="cat-container">
			<div class="main-cat">
				<input type="hidden" name="feed_preference[parenting]" id="_cat-parenting" value="0"/>
				<input type="checkbox" name="feed_preference[parenting]" class="cb-main-cat" id="cat-parenting" value="1" <?php if(isset($feed_preference['parenting'])&&$feed_preference['parenting']){?>checked="checked" <?php }?>/>
				<label for="cat-parenting">Parenting</label>
			</div>
		</div>
	</div>
	
<?php 
}

add_action( 'personal_options_update', 'rr_save_feed_profile_fields' );
add_action( 'edit_user_profile_update', 'rr_save_feed_profile_fields' );

function rr_save_feed_profile_fields( $user_id ) {

	if ( !current_user_can( 'edit_user', $user_id ) )
		return false;
		
	$feed_preference = $_POST['feed_preference'];
	
	update_user_meta($user_id, 'feed_preference', $feed_preference);

}

function rr_load_scripts_css() {
	
	wp_register_script('main_script', plugins_url("js/main_script.js", __FILE__), array(), '1.0.0');
	
	wp_register_style( 'custom-style', plugins_url('css/user_meta.css', __FILE__) );
	// enqueing:
	wp_enqueue_script('main_script');
	wp_enqueue_style( 'custom-style' );
	
}

add_action( 'wp_enqueue_scripts', 'rr_load_scripts_css' );
add_action( 'admin_enqueue_scripts', 'rr_load_scripts_css' );

?>