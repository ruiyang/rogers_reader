<?php
/**
 * Visual functions and definitions
 *
 * @package Visual
 * @since Visual 0.1
 */
	 
/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since Visual 0.1
 */
if ( ! isset( $content_width ) )
	$content_width = 670; /* pixels */
	
/**
 * Loads Options Framework for theme options
 * See: https://github.com/devinsays/options-framework-theme
 *
 * @since Visual 0.4
 */
function visual_optionsframework_setup() {
	if ( !function_exists( 'optionsframework_init' ) ) {
		define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/inc/options-framework/' );
		require_once dirname( __FILE__ ) . '/inc/options-framework/options-framework.php';
	}
}
add_action( 'after_setup_theme', 'visual_optionsframework_setup' );

/*
 * Load Jetpack compatibility file.
 */
require( get_template_directory() . '/inc/jetpack.php' );

if ( ! function_exists( 'visual_setup' ) ) :

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 *
 * @since Visual 0.1
 */
function visual_setup() {

	/**
	 * Custom template tags for this theme.
	 */
	require( get_template_directory() . '/inc/template-tags.php' );

	/**
	 * Custom functions that act independently of the theme templates
	 */
	require( get_template_directory() . '/inc/extras.php' );
	
	/**
	 * Loads options for theme customizer
	 */
	require_once( get_template_directory() . '/inc/options.php' );
	
	/**
	 * Functions to enable the options
	 */
	require( get_template_directory() . '/inc/options-functions.php' );

	/**
	 * Customizer additions
	 */
	require( get_template_directory() . '/inc/customizer.php' );

	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 * If you're building a theme based on Visual, use a find and replace
	 * to change 'visual' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'visual', get_template_directory() . '/languages' );

	/**
	 * Add default posts and comments RSS feed links to head
	 */
	add_theme_support( 'automatic-feed-links' );

	/**
	 * Enable support for Post Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'visual-thumbnail', 328, 999 );
	add_image_size( 'visual-post', 700, 9999 );

	/**
	 * This theme uses wp_nav_menu() in one location.
	 */
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'visual' ),
	) );

	/**
	 * Enable support for Post Formats
	 */
	add_theme_support( 'post-formats', array( 'image', 'gallery' ) );
}
endif; // visual_setup

add_action( 'after_setup_theme', 'visual_setup' );

/**
 * Register widgetized area and update sidebar with default widgets
 *
 * @since Visual 0.1
 */
 
function visual_widgets_init() {
	register_sidebar( array(
		'name' => __( 'Sidebar', 'visual' ),
		'id' => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h1 class="widget-title"><span>',
		'after_title' => '</span></h1>',
	) );
}

add_action( 'widgets_init', 'visual_widgets_init' );

/**
 * Enqueue scripts and styles
 *
 * @since Visual 0.1
 */
 
function visual_scripts() {

	wp_enqueue_style( 'style', get_stylesheet_uri() );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20130220' );
	}
	
	if ( !is_singular() && !is_404() && !is_author() ) {
		wp_enqueue_script( 'visual-masonry', get_template_directory_uri() . '/js/jquery.masonry.min.js', array( 'jquery' ), '20130220', true );
	}
	
	wp_enqueue_script( 'visual-scripts', get_template_directory_uri() . '/js/visual-scripts.js', array( 'jquery' ), '20130220', true );
	
}

add_action( 'wp_enqueue_scripts', 'visual_scripts' );

/**
 * Enqueue fonts
 *
 * @since Visual 0.1
 */

function visual_fonts() {
		$font_families = array();
		$font_families[] = 'Raleway:400,700';
		$protocol = is_ssl() ? 'https' : 'http';
		$query_args = array(
			'family' => implode( '|', $font_families ),
			'subset' => 'latin,latin-ext',
		);
		wp_enqueue_style( 'visual-fonts', add_query_arg( $query_args, "$protocol://fonts.googleapis.com/css" ), array(), null );
}
add_action( 'wp_enqueue_scripts', 'visual_fonts' );

/**
 * Adds a body class for masonry layouts
 *
 * @since Visual 0.1
 */
 
function visual_body_class( $classes ) {
	if ( !is_singular() && !is_404() && !is_author() )
		$classes[] = 'masonry';
	return $classes;
}

add_filter('body_class','visual_body_class');

/**
 * Loads options.php from "inc" directory
 *
 * @since Visual 0.3
 */
 
function visual_options_location() {
	return array('/inc/options.php');
}

add_filter ( 'options_framework_location', 'visual_options_location' );


/**
 * Selects feeds based on tags from user settings
 * returns an array of post_ids to be displayed on front page
 */

function visual_select_feeds_by_tags( $user_tags ) {
	global $wp_query;
	$post_ids = array();
	$index = 0;
	foreach( $wp_query->posts as $post) { 				//loops through entire posts
		$post_tag = visual_get_tag_by_id($post->ID);	//get the tag name for each post
		if ( isset($user_tags[$post_tag]) && $user_tags[$post_tag] == 1 ) {			//check if user has that tag enabled
			$post_ids[$index] = $post->ID;				//if enabled, add post id to $post_ids
			$index++;
		}
	}
	return $post_ids;
}

function visual_display_this() {
	global $wp_query;
		foreach( $wp_query->posts as $post) { 				//loops through entire posts
			$post_tag = visual_get_tag_by_id($post->ID);	//get the tag name for each post
		}
}


/*
 * Return the post tag from post id
 */

function visual_get_tag_by_id( $id ) {
  $tags = wp_get_post_tags($id);
 // print_r($tags);
 if(isset($tags[0])){
  return $tags[0]->slug;
 }else{
  return 0;
 }
}
