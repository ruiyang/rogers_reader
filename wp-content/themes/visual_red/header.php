<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package Visual
 * @since Visual 0.1
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<link rel="icon" 
      type="image/png" 
      href="<?php echo get_template_directory_uri(); ?>/img/favicon.png">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->

<?php wp_head(); ?>

<script src="http://code.jquery.com/jquery-1.10.0.min.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>



<!-- Ad Utility source code begins -->

        <script type="text/javascript" src="http://static.rogersdigitalmedia.com/libs/RDMAdUtility4/current/rdm-ad-util.min.js"></script> 

	<script type='text/javascript'>	
		window.adUtility = new RDMAdUtility({
			site: 'rogers.publishing/moneysense.CPGBusiness',
			zone: 'homepage',
			sponsorshipId: ''
		});
	</script>

<!-- Ad Utility source code ends -->




</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<?php do_action( 'before' ); ?>
	<header id="masthead" class="site-header" role="banner">
		<div class="section clearfix">
			<hgroup>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			</hgroup>
	
			<nav role="navigation" class="site-navigation main-navigation clearfix">
				<h3 class="assistive-text menu-toggle"><a class="icon-menu" href="#menu-main"><?php _e( 'Menu', 'visual' ); ?></a></h3>

			

			<div class="assistive-text skip-link"><a href="#content" title="<?php esc_attr_e( 'Skip to content', 'visual' ); ?>"><?php _e( 'Skip to content', 'visual' ); ?></a></div>


			<?php
			if ( has_nav_menu( 'primary' ) ) {
				wp_nav_menu( array( 'theme_location' => 'primary', 'walker' => new Visual_Nav_Walker(), 'depth' => '2' ) );
			} else {
				wp_page_menu();
			}
			?>



			</nav><!-- .site-navigation .main-navigation -->

		


		</div>



	</header><!-- #masthead .site-header -->




	<div id="main" class="site-main">
		<div class="section clearfix">

<form method="get" action="<?php echo get_site_url(); ?>/">
			<div class="header-search">
				<input type="search" class="header-searchbox" label="Search" name="s" results="5" title="Search" value="<?php echo esc_attr($_GET['s']); ?>" />
			</div>
		</form>


<script type="text/javascript">

$(document).ready(function() {

	$("label").click(function() {

		var tag = $(this).attr('id');

		$('#content').children('article').each(function() {
			if ($(this).hasClass('tag-' + tag)) {

				$(this).toggle();
			}
		});
	});
});


</script>


<form>
	<div class="header-choices">
		<b>Display:</b>
		<label id = "news"		><input type="checkbox" id="news" 		name="news" 	checked="checked" />		News</label>
		<label id = "sports"	><input type="checkbox" id="sports"	name="sports"	checked="checked" />	Sports</label>
		<label id = "business"	><input type="checkbox" id="biz" 		name="biz" 		checked="checked" />		Business</label>
		<label id = "finance"	><input type="checkbox" id="finance"	name="finance"	checked="checked" />	Finance</label>
		<label id = "womens"	><input type="checkbox" id="women"		name="women"	checked="checked" />		Women's</label>
		<label id = "parenting"	><input type="checkbox" id="parent"	name="parent"	checked="checked" />	Parenting</label>
	</div>
</form>

		</div>
	</div>





	<div id="main" class="site-main">
		<div class="section clearfix">
