<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visual
 * @since Visual 0.1
 */



$space_ads_every = 4;
$space_ads_count = 0;


get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php if ( have_posts() ) : ?>

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php
						/* Include the Post-Format-specific template for the content.
						 * If you want to overload this in a child theme then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'content', get_post_format() );

						$space_ads_count++;




					if($space_ads_count % $space_ads_every == 0){
						echo "<!-- INSERT AD HERE, COUNT = ".$space_ads_count." BEGINS -->";
					?>


					<article class="post-ad post type-post status-publish format-standard hentry category-ad masonry-brick" style="text-align: center; padding-top: 20px; padding-bottom: 20px;">
						Advertisement
						 <script type="text/javascript">
		          		<?php /*	adUtility.insertAd("bigbox-1369958995-331", {type: adUtility._AD_BIGBOX});	*/ ?>
		          			adUtility.insertAd("bigbox-1369958995-331<?php echo $space_ads_count;?>", {type: adUtility._AD_BIGBOX});
						</script>
					</article>

					<?
						echo "<!-- INSERT AD HERE, COUNT = ".$space_ads_count." ENDS -->";
					}


			


					?>

				<?php endwhile; ?>

			<?php else : ?>

				<?php get_template_part( 'no-results', 'index' ); ?>

			<?php endif; ?>

			</div><!-- #content .site-content -->
			
		<?php visual_content_nav( 'nav-below' ); ?>
			
		</div><!-- #primary .content-area -->

<?php get_footer(); ?>